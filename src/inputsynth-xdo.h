/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef __INPUT_SYNTH_XDO_H__
#define __INPUT_SYNTH_XDO_H__

#include <glib-object.h>
#include "inputsynth.h"
#include <xdo.h>
#include <gmodule.h>

G_BEGIN_DECLS

#define INPUT_TYPE_SYNTH_XDO input_synth_xdo_get_type()
G_DECLARE_FINAL_TYPE (InputSynthXdo, input_synth_xdo, INPUT, SYNTH_XDO, InputSynth)

struct _InputSynthXdoClass
{
  InputSynthXdoClass parent;
};

G_MODULE_EXPORT InputSynthXdo *input_synth_xdo_new (void);

G_END_DECLS

#endif /* __INPUT_SYNTH_XDO_H__ */
