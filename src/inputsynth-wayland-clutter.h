/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef __INPUT_SYNTH_XDO_H__
#define __INPUT_SYNTH_XDO_H__

#include <glib-object.h>
#include "inputsynth.h"
#include <gmodule.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define INPUT_TYPE_SYNTH_WAYLAND_CLUTTER input_synth_wayland_clutter_get_type()
G_DECLARE_FINAL_TYPE (InputSynthWaylandClutter, input_synth_wayland_clutter, INPUT, SYNTH_WAYLAND_CLUTTER, InputSynth)

struct _InputSynthWaylandClutterClass
{
  InputSynthWaylandClutterClass parent;
};

G_MODULE_EXPORT
InputSynthWaylandClutter *input_synth_wayland_clutter_new (void);


G_END_DECLS

#endif /* __INPUT_SYNTH_XDO_H__ */
